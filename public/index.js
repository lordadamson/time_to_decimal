function _on_input_changed()
{
    const error_p = document.getElementById("error_p");
    const time = document.getElementById("time").value;
    const decimal = document.getElementById("decimal");

    error_p.innerHTML = "";
    decimal.innerHTML = "";

    const lines = time.split('\n');

    var sum = 0.0;

    for(var i = 0; i < lines.length; i++)
    {
        if(lines[i] == "")
        {
            continue;
        }

        const tokens = lines[i].split(':');

        if(tokens.length != 2)
        {
            error_p.innerHTML = "This line is not formatted correctly: " + lines[i];
            return;
        }

        const hours = Number(tokens[0]);

        if(Number.isNaN(hours) || hours < 0 || tokens[0] == "")
        {
            error_p.innerHTML = "This line is not formatted correctly: " + lines[i];
            return;
        }

        const minutes = Number(tokens[1]);

        if(Number.isNaN(minutes) || minutes < 0 || minutes > 60 || tokens[1] == "")
        {
            error_p.innerHTML = "This line is not formatted correctly: " + lines[i];
            return;
        }

        sum += hours + (minutes / 60);
    }

    decimal.innerHTML = sum.toFixed(2);
}